Задание выполнено на фреймвоке Laravel
(в тестовом не было указано на чём делать, если что могу переделать на самопис)
1) Развернуть проект локально
2) файл .env.example копировать и переименовать в .env изменить параметры: DB_HOST, DB_DATABASE(testbetaservice), DB_USERNAME, DB_PASSWORD
3) файл с экспортом бд - backup.sql
4) в директории выполнить команду "composer install" и "php artisan key:generate"

пользователи приложения: 
л - test@test.test
п - testtest

л - test2@test2.test2
п - test2test2
